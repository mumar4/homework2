The design pattern I had was the mediator design pattern. Taking example code I 
incorporated the mediator design pattern. I researched the design pattern and found that
it loosely couples objects that may be very couples beforehand. In every implementation I found 
a couple things to be true. 

1. The pattern always has a mediator interface
2. The mediator interface is defined by a concrete mediator class
3. The entities to mediate are broken down into abstract classes
4. Those abstract classes are concretely defined
5. In each of thee concrete classes is the mediator object

This is important for the objects to both communicate with the mediator. I first decided
to annotate each part of the mediator pattern, however I did not get far in the actual
processing of the annotations. I spent most of my time creating the environment and researching
ad did not get to as much implementation as I wanted. I basically just defined basic annotations 
and implemented a mediator design pattern using online resources. I made modules for the 
annotation processor and the main annotation processor I tried to implement was MediatorProcessor but could not due to technical difficulty. 
