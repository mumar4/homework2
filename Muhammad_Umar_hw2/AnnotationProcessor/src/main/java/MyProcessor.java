
//import annotation.Setter;
import jdk.nashorn.internal.objects.annotations.Setter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;

//SupportedAnnotationTypes({"annotation.Setter"})
public class MyProcessor extends AbstractProcessor {

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment env) {
        Messager messager = processingEnv.getMessager();
        for (TypeElement typeElement : annotations) {
            for (Element element : env.getElementsAnnotatedWith(typeElement)) {
                DateFormat annotation = element.getAnnotation(DateFormat.class);
                MediatorVar annotation1 = element.getAnnotation(MediatorVar.class);
                TypeMirror typeMirror = element.asType();

                messager.printMessage(Diagnostic.Kind.ERROR,
                           annotation.toString());
                System.out.println(annotation1.toString());
//                if((annotation1.getClass().toString()){
//                    messager.printMessage(Diagnostic.Kind.ERROR,
//                            "Not an interface");
//                }
                //checking the filed type
                //we don't have field class type information as code is not yet built
                //so we have to compare the strings
                if (!typeMirror.toString().equals(LocalDate.class.getName())) {
                    messager.printMessage(Diagnostic.Kind.ERROR,
                            "The type annotated with @DateFormat must be LocalDate");
                }

                try {
                    //validating date format
                    DateTimeFormatter simpleDateFormat =
                            DateTimeFormatter.ofPattern(annotation.value());
                    LocalDate.now().format(simpleDateFormat);

                } catch (Exception e) {
                    messager.printMessage(Diagnostic.Kind.ERROR,
                            "Not a valid date format " + annotation.value());
                }
            }
        }
        return false;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return new HashSet<>(Arrays.asList(DateFormat.class.getName()));
    }
}