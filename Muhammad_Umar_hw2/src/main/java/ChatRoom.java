import jdk.nashorn.internal.objects.annotations.Setter;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

//CONCRETE MEDIATOR
@ConcreteMediator
public class ChatRoom implements IChatRoom {

    private Map<String, User> usersMap = new HashMap<>();


    @Override
    public void sendMessage(String msg, String userId)
    {
        User u = usersMap.get(userId);
        u.receive(msg);
    }

    @Override
    public void addUser(User user) {
        this.usersMap.put(user.getId(), user);
    }
}